export const PROMOCIONES = [
    {
      id: 0,
      name: 'Día de la madre',
      image: '/assets/images/11.jpg',
      label: 'New',
      price: '7.99',
      featured: true,
      description: 'Por el día de la madre disfruta de tu bebida favorita para ti y tu madre solo por $7.99'
    }
  ];