export const CHEFS = [
    {
      id: 0,
      name: 'Peter Pan',
      image: '/assets/images/chef.jpg',
      designation: 'Chef Ejecutivo',
      abbr: 'CE',
      featured: true,
      description: "Chef reconocido nacional e internacionalmente, ganador de estrella Michelin"
    }
  ];