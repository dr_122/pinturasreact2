export const COMENTARIOS = 
[
   {
            id: 0,
		dishId: 0,
            rating: 10,
            comment: "Acabo de contemplar la famosa pintura de la Mona Lisa y estoy fascinado. Es una experiencia totalmente distinta a verla en reproducciones o en imágenes. Los detalles sutiles y la enigmática sonrisa de la Mona Lisa cobran vida cuando estás frente a ella ",
            author: "Sergio Fernandez",
            date: "2012-10-16T17:57:28.556094Z"
   },
   {
            id: 1,
		dishId: 0,
            rating: 9,
            comment: "Esta experiencia en la exposición me recordó lo poderoso que puede ser el arte para transmitir ideas, emociones y provocar el pensamiento crítico. Me siento agradecido de haber tenido la oportunidad de sumergirme en este mundo creativo y contemplar la diversidad de voces y perspectivas que el arte contemporáneo ofrece.",
            author: "Jony Jara",
            date: "2014-09-05T17:57:28.556094Z"
    },
    {
            id: 2,
		dishId: 1,
            rating: 10,
            comment: "Los vibrantes colores y pinceladas enérgicas crean un cielo nocturno hipnótico y lleno de movimiento. Las estrellas parecen danzar y brillar intensamente sobre el paisaje nocturno. Me sentí transportado a esa escena, sintiendo la calma y la majestuosidad de la noche.",
            author: "Carla A",
            date: "2012-10-16T17:57:28.556094Z"
     },
     {
            id: 3,
		dishId: 1,
            rating: 8,
            comment: "Esta obra maestra continúa impactando a los espectadores con su belleza atemporal y su representación emocionalmente cargada. Es verdaderamente una joya del arte que perdurará en la historia y en el corazón de todos aquellos que la contemplan.",
            author: "Miguel Reinoso",
            date: "2014-09-05T17:57:28.556094Z"
      },
      {
            id: 4,
		dishId: 1,
            rating: 10,
            comment: "Mientras escuchaba musica en la galeria mire esta pintura y me senti algo relajado y melancolico es una pintura muy hermosa.",
            author: "Miguel Acosta",
            date: "2015-02-13T17:57:28.556094Z"
      },
      {
            id: 5,
		dishId: 2,
            rating: 8,
            comment: "Cuando vi la obra de el grito pude contemplar como se nota esa expresion de terror de una persona que esta aterrada por algo como se mira la pintura con lienzos esteticos y ondulados haciendolo mejor, en el tiempo que se pinto esta pintura me sorprende lo muy bien cuidada que esta, simplemente maravillosa tal ves venga el proximo año con mi familia a la galeria de arte.",
            author: "Carlos Acosta",
            date: "2016-11-30T19:47:28.556094Z"
      },
      {
            id: 6,
		dishId: 3,
            rating: 5,
            comment: "Es algo que siempre me ha gustado de las pinturas de Salvador Dali que refleja un realismo muy genial, tambien ya que ha hecho muchas obras asi es sorprendente lo que pueden hacer los artistas, tambien he escuchado la historia de Dali y simplemente es algo espectacular.",
            author: "james Díaz",
            date: "2018-03-09T09:27:28.556094Z"
      },
      {
            id: 7,
		dishId: 4,
            rating: 7,
            comment: "Ayer fui a la galeria y mire la maravillosa obra de la creacion de Adan fue espectacular me encanto mucho ya que el artista Michelangelo Buonarroti siempre ha utilizado en sus fondos organos humanos si miran con detalle en la parte donde esta Dios se ve que esta en la figura de un cerebro, tambien es increible de como si Dios bajara al cielo y creara a Adan para seguir sus pasos y convertirse en algun momento en un Dios.",
            author: "Peter Rupert",
            date: "2015-07-27T20:50:28.556094Z"
      },
      {
            id: 8,
		dishId: 4,
            rating: 4,
            comment: "Acabo de observar la pintura de la creación de Adán de Miguel Ángel y es simplemente impresionante. La forma en que los dedos de Dios y Adán se acercan sin llegar a tocarse transmite una sensación de anticipación y conexión divina. Es una representación poderosa y llena de simbolismo que deja una impresión duradera.",
            author: "July Ruiz",
            date: "2017-11-15T16:06:28.556094Z"
      },
      {
            id: 9,
		dishId: 5,
            rating: 4,
            comment: "Es espectacular como se guia en la mitologia griega sobre medusa cortada la cabeza",
            author: "Nicolás Herrera",
            date: "2018-03-17T18:30:28.556094Z"
      },
      {
            id: 10,
		dishId: 5,
            rating: 3,
            comment: "La cabeza de meduza es interesante debido a que en este siempre se ha referenciado en muchos juegos con God of war en el cual puedes usar su cabeza para hacer que el tiempo se detenga.",
            author: "Felipe Rubidio",
            date: "2019-12-11T15:09:28.556094Z"
      },
      {
            id: 11,
		dishId: 5,
            rating: 4,
            comment: "La pintura de la cabeza de Medusa es un ejemplo impresionante de cómo el arte puede transmitir emociones intensas. El realismo detallado de las serpientes y la expresión de terror en el rostro de Medusa generan una sensación de intriga y temor.",
            author: "Nelson Terán",
            date: "2020-04-19T11:09:28.556094Z"
      },
      {
            id: 12,
		dishId: 6,
            rating: 5,
            comment: "Excelente detalle vi hace unos dias en un anuncio de que la galeria tenia las pinturas mas famosas del mundo y quise venir y poder tomar fotos de como era en persona.",
            author: "Roberto Vinueza",
            date: "2017-08-29T21:28:28.556094Z"
      },
      {
            id: 13,
		dishId: 6,
            rating: 10,
            comment: "Soy un artista que esta empezando cuando mire la pintura de Rembrandt me encanto por los detalles que le puso no tengo idea de cuanto tiempo le tomo dibujar este hermoso cuadro p",
            author: "Martin Goodly",
            date: "2018-05-10T17:08:28.556094Z"
      },
      {
            id: 14,
		dishId: 6,
            rating: 8,
            comment: "La pintura es demasiado hermosa",
            author: "Lina Enríquez",
            date: "2018-02-27T16:26:28.556094Z"
      },
      {
            id: 15,
		dishId: 6,
            rating: 5,
            comment: "Me encanto toda la exposicion de la galeria y esta en si es mi pintura favorita de todas, aunque las otras son mejores que esta.",
            author: "Jonnhy",
            date: "2018-07-05T19:20:28.556094Z"
      }
]