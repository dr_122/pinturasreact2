export const GALERITAS =
    [
        {
        id: 0,
        name: 'La Mona Lisa',
        image: '/assets/images/1.jpg',
        category: 'arte de Renacimiento',
        label: 'Frío',
        price: '2.50',
 	  featured: true,
        description: 'La Mona Lisa es una famosa pintura renacentista creada por Leonardo da Vinci. También conocida como La Gioconda, representa a una mujer con una expresión enigmática y una mirada cautivadora. ',
        },
        {
        id: 1,
        name:'Noche estrellada',
        image: '/assets/images/2.jpg',
        category: 'Arte contemporaneo',
        label:'Caliente',
        price:'4.00',
 	  featured: false,
        description:'La Noche Estrellada es una famosa pintura del postimpresionista Vincent van Gogh. La obra representa un paisaje nocturno con un cielo estrellado dominado por remolinos de colores intensos. '
        },
        {
        id: 2,
        name: 'El Grito',
        image: '/assets/images/3.jpg',
        category: 'bebidas',
        label: 'Caliente',
        price: '2.00',
 	  featured: false,
        description: 'El Grito es una famosa pintura del expresionista noruego Edvard Munch. La obra representa una figura angustiada de pie en un puente con un fondo de paisaje turbulento. La figura tiene la boca abierta en una expresión de desesperación.'                      
        },
        {
        id: 3,
        name: 'La persistencia de la memoria',
        image: '/assets/images/4.jpg',
        category: 'postres',
        label: 'Al clima',
        price: '3.25',
 	  featured: false,
        description: 'La Persistencia de la Memoria es una famosa pintura del surrealista español Salvador Dalí. La obra muestra un paisaje desértico con relojes derretidos y distorsionados que cuelgan de árboles y rocas. Estos relojes blandos representan la relatividad del tiempo y la fugacidad de la realidad'
        },
        {
        id: 4,
        name: 'La Creación de Adán',
        image: '/assets/images/5.jpg',
        category: 'postres',
        label: 'Al clima',
        price: '1.60',
 	  featured: false,
        description: 'La Creación de Adán es una famosa pintura del techo de la Capilla Sixtina, en el Vaticano, creada por el renacentista italiano Michelangelo Buonarroti. La obra representa el momento en que Dios, representado como una figura mayor y poderosa, extiende su mano hacia Adán, el primer hombre.'
        },
        {
        id: 5,
        name: 'La cabeza de medusa',
        image: '/assets/images/6.jpg',
        category: 'fuerte',
        label: 'Caliente',
        price: '7.99',
 	  featured: false,
        description: 'La pintura más conocida es "La Medusa" de Caravaggio, que retrata la cabeza decapitada de Medusa con serpientes en lugar de cabello. La expresión de terror y la mirada penetrante de los ojos de Medusa son capturadas vívidamente en la obra. '
        }, 
        {
        id: 6,
        name: 'La ronda de noche',
        image: '/assets/images/7.jpg',
        category: 'fuerte',
        label: 'Caliente',
        price: '6.50',
 	  featured: false,
        description: 'La ronda de noche es una famosa pintura del artista holandés Rembrandt van Rijn. También conocida como La compañía del capitán Frans Banning Cocq y el teniente Willem van Ruytenburch'
        }                          
    ];