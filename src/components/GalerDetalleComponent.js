import React from "react";
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem} from "reactstrap";
import { Link } from "react-router-dom";

function RenderGaler({galer}){
    return(
        <div className="col-12 col-md-5 m-1">
            <Card>
                <CardImg width="100%" src={galer.image} alt={galer.name} />
                <CardBody>
                    <CardTitle>{galer.name}</CardTitle>
                    <CardText>{galer.description}</CardText>
                </CardBody>
            </Card>
        </div>
    );
}
function RenderComentarios({comentarios}){
    if(comentarios !=null)
        return(
            <div className="col-12 col-md-5 m-1">
                <h2>Comentarios</h2>
                <ul className="list-unstyled">
                    {comentarios.map((comentario)=>{
                        return(
                            <div key={comentario.id}>
                                <li>
                                    <p>{comentario.comment}</p>
                                    <p>{comentario.rating}</p>
                                    <p>{comentario.author}, {new Intl.DateTimeFormat("en-Us", {year: "numeric", month: "short", day: "2-digit"}).format(new Date (Date.parse(comentario.date)))}</p>
                                </li>
                            </div>
                        );
                    })}
                </ul>
            </div>
    );
}

const GalerDetalle=(props) => {
    if(props.galer !=null)
        return(
            <div className="container">
                <div className="row">
                <Breadcrumb>
                        <BreadcrumbItem><Link to='/galeria'>Galeria</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.galer.name}</BreadcrumbItem>
                    </Breadcrumb>
                </div>
                <div className="col-12">
                    <h3>{props.galer.name}</h3>
                    <hr />
                </div>
                <div className="row">
                    <RenderGaler galer={props.galer}/>
                    <RenderComentarios comentarios={props.comentarios}/>
                </div>
            </div>
        );
    else
        return(
            <div></div>
        );
}
export default GalerDetalle;