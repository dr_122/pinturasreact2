import React from 'react';
import {Link} from 'react-router-dom';

function Footer(props){
    return(
        <div className='footer'>
            <div className='container'>
                <div className='row justify-content-center'>
                    <div className='col-4 offset-1 col-sm-2'>
                        <h5>Enlaces Importantes</h5>
                        <ul className='list-unstyled'>
                            <li><Link to="./home">Home</Link></li>
                            <li><Link to="./acercade">Acerca de</Link></li>
                            <li><Link to="./galeria">Galeria</Link></li>
                            <li><Link to="./contactos">Contactos</Link></li>
                        </ul>
                    </div>
                    <div className='col-7 col-sm-5'>
                        <h5>Nuestra Dirección: </h5>
                        <address>
                            Cotacachi<br />
                           Av Salinas y Pedro Moncayo<br />
                            El Sagrario<br />
                            <i className='fa fa-phone fa-lg'>0997655443</i>
                            <i className='fa fa-envelope fa-lg'></i>: <a href="dsruizq@utn.edu.ec">dsruizq@utn.edu.ec</a>
                        </address>
                    </div>
                    <div className='col-12 col-sm-4 align-self-center'>
                        <div className='text-center'>
                            <a className='btn btn-social-icon btn-facebook' href='http://facebook.com/'><i className='fa fa-facebook'></i></a>
                        </div>
                    </div>
                </div>
                <div className='row justify-content-center'>
                    <div className='col-auto'>
                        <p>Derechos Reservados 2023 / Galeria de arte Darlyn</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Footer;