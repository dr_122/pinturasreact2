import React, {Component} from 'react';
import {Navbar, NavbarBrand, Nav, NavbarToggler, Collapse,NavItem} from 'reactstrap';
import {NavLink} from 'react-router-dom';

class Header extends Component{
    constructor (props){
        super(props);
        this.state={
            isNavOpen:false
        };
        this.toggleNav=this.toggleNav.bind(this);
    }
    toggleNav(){
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }

    render(){
        return(
            <React.Fragment>
                <Navbar dark expand="md">
                    <div className='container'>
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className='mr-auto' href='/'>
                            <img src="assets/images/15.jpg" height="25" width="25" alt="Galeria Darlyn"/>
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                                <NavItem>
                                    <NavLink className='nav-link' to="/home">
                                        <span className='fa fa-home fa-lg'></span>Home
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to="/acercade">
                                        <span className='fa fa-info fa-lg'></span>Acerca de
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to="/galeria">
                                        <span className='fa fa-list fa-lg'></span>Galeria
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to="/contact">
                                        <span className='fa fa-address-card fa-lg'></span>Contactos
                                    </NavLink>
                                </NavItem>                                                                                                
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <div className='jumbotron'>
                    <div className='container'>
                        <div className='row row-header'>
                            <div className='col-12 col-sm-6'>
                                <h1>Galeria de arte Darlyn</h1>
                                <p>¡Bienvenidos a nuestra maravillosa galería de arte! Sumérgete en un mundo de creatividad y belleza mientras exploras nuestras exquisitas exhibiciones. Cada sala te transportará a un universo único de colores vibrantes, formas cautivadoras y emociones que te conmoverán.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default Header;