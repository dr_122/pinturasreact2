import React, { Component } from 'react';
import Galeria from './GallerComponent';
import Contact from './ContactComponent';
import { GALERITAS } from '../shared/galeritas';
import { COMENTARIOS } from '../shared/comentarios';
import { PROMOCIONES } from '../shared/promociones';
import { CHEFS } from '../shared/chefs';
import GalerDetalle from "./GalerDetalleComponent";
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent'; 
import {Routes, Route, useParams} from 'react-router-dom';

class Main extends Component {
  constructor(props){
    super(props);
    this.state={
        galers: GALERITAS,
        comentarios: COMENTARIOS,
        promociones: PROMOCIONES,
        chefs: CHEFS
    };
  }

  render(){
    const HomePage=() => {
      return(
        <Home 
          galer = {this.state.galers.filter((galer) => galer.featured)[0]}
          promocion = {this.state.promociones.filter((promo) => promo.featured)[0]}
          chef = {this.state.chefs.filter((cocinero) => cocinero.featured)[0]}
        />
      );
    }

    const GalerConId=() => {
      const params = useParams();
      return(
        <GalerDetalle 
          galer={this.state.galers.filter((galer) => galer.id === parseInt(params.dishId, 10))[0]} 
          comentarios={this.state.comentarios.filter((comentario) => comentario.dishId === parseInt(params.dishId, 10))}
        />
      );
    }

    return (
      <div>
      <Header /> 
      <Routes>
        <Route path="/home" element={<HomePage/>} />
        <Route path="/galeria" element={<Galeria galers={this.state.galers} />}/>
        <Route path='/galeria/:dishId' element={<GalerConId/>} />
        <Route path="/contact" element={<Contact/>}/>
        <Route to="/home"/>
      </Routes>
      <Footer />
    </div>
  );
}
}
export default Main;