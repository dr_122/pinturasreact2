import React from 'react';

function Contact(props) {
  return (
    <div className='container'>
      <div className='row row-content'>
        <div className='col-12'>
          <h3>Informacion</h3>
        </div>
        <div className='col-12 col-sm-4 offset-sm-1'>
          <h5>Nuestra dirección</h5>
          <address>
            Cotacachi <br />
            Av. Salinas y Pedro Moncayo <br />
            El Sagrario<br />
            <i className='fa fa-phone'></i> +593 098553290 <br />
            <i className='fa fa-envelope'></i>
            <a href='mailto:dsruizq@utn.edu.ec'>dsruizq@utn.edu.ec</a>
          </address>
        </div>
        <div className='col-12 col-sm-6 offset-sm-1'>
          <h5>Mapa de nuestra ubicación</h5>
        </div>
        <div className='col-12 col-sm-6 offset-sm-1'>
          <div className='btn-group' role='group'>
            <a role='button' className='btn btn-primary' href='tel:+593099567345'>
              <i className='fa fa-phone'></i>Llamar
            </a>
          </div>
        </div>
        <div className='col-12'>
          <h5>Descripción</h5>
          <p>Puedes contactarnos si gustas atendemos todos los dias de Lunes a Viernes</p>
        </div>
      </div>
    </div>
  );
}

export default Contact;