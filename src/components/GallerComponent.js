import React from "react";
import { Card, CardImg, CardImgOverlay, CardTitle, Breadcrumb, BreadcrumbItem } from "reactstrap";
import {Link} from 'react-router-dom';

    function RenderGallerItem({galer}){
        return (
            <Card>
                <Link to={`/galeria/${galer.id}`}>
                <CardImg width="100%" src={galer.image} alt={galer.name} />
                    <CardImgOverlay>
                        <CardTitle>{galer.name}</CardTitle>
                     </CardImgOverlay>
                </Link>
            </Card>
        );
    }

    const Galeria=(props) => {
        const galeria= props.galers.map((galer) => {
            return(
                <div key={galer.id} className="col-12 col-md-5 m-1">
                    <RenderGallerItem galer={galer} />
                </div>
            );
        });
       return(
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Galeria</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Galeria</h3>
                    <hr />
                </div>
            </div>
            <div className="row">
                {galeria}
            </div>
        </div>
       ); 
    }
export default Galeria;
